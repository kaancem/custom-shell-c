/**
* KUSH shell interface program
*/

#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <string.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <string.h>
// #include <linux/include.h>


#define MAX_LINE       80 /* 80 chars per line, per command, should be enough. */
#define CRONFILE "cronfile" /* cronfile name */
#define HIST_BUFFER_SIZE 100 /* Maximum recent commands to store or histroy command */
#define SCHEDINFO "task"
#define SCHEDINFO_PATH "/home/kaan/Desktop/School/Comp304/Project1/task.ko"

int parseCommand(char inputBuffer[], char *args[],int *background);
int executeCommand(char *args[]);
int pipeIndex(char *args[]);
int redirect(char *args[],int *fileDescriptor);
int finit_module(int fd, const char *param_values, int flags);
int delete_module(const char *name, int flags);


// Custom history command related variables.
char history[HIST_BUFFER_SIZE][MAX_LINE]; /* Buffer for contatingg history */
int hst = 0; /* Marker for index of the history array */
int hstCirc = 0;  /* Marker for real size of history*/
int insmod_called = 0;
pid_t insmod_called_with = -1;


int main(void){
    char inputBuffer[MAX_LINE];           /* buffer to hold the command entered */
    int background;                       /* equals 1 if a command is followed by '&' */
    char *args[MAX_LINE/2 + 1];           /* command line (of 80) has max of 40 arguments */
    pid_t child;                  /* process id of the child process */
    int status;                   /* result from execv system call*/
    int shouldrun = 1;
    
    // New variables declared
    int i;

    // Variables related to pipe.
    int pipe_i = 0;
    int pipefd[2];


    while (shouldrun){                    /* Program terminates normally inside setup */
        background = 0;
        shouldrun = parseCommand(inputBuffer,args,&background);       /* get next command */

        if (args[0]) {
            i = 1;
            strcpy(history[hst], args[0]);
            while (args[i] !=NULL) {
                sprintf(history[hst], "%s %s", history[hst], args[i++]);
            }
            hst++;
            hstCirc++;
            hst = hstCirc % HIST_BUFFER_SIZE;
        }

        if (strncmp(inputBuffer, "exit", 4) == 0)
            shouldrun = 0;     /* Exiting from kush*/

        if (shouldrun) {
            if (!strcmp(args[0], "cd")) {
                if (chdir(args[1] ? args[1] : "/home/kaan"))
                    perror("chdir");
            } else if(!strcmp(args[0], "schedinfo")){
                if(args[1] == NULL || args[2] == NULL || args[3] == NULL){
                    fprintf(stderr, "schedinfo: Missing argument\n");
                } else {
                    char schedArgs[1024];
                    int sched_pid = atoi(args[1]);
                    int policy = atoi(args[2]);
                    int prio = atoi(args[3]);
                    if(insmod_called && insmod_called_with == sched_pid)
                        printf("schedinfo: Already called with this PID.\n");
                    else {
                        if(insmod_called){
                            int delete_result = delete_module(SCHEDINFO, 0);
                            if (delete_result != 0)
                                printf("Can't remove existsing module: %s. skipping...", strerror(errno));
                            
                            insmod_called = 0;
                            insmod_called_with = -1;
                        }

                        sprintf(schedArgs, "processID=%d processPolicy=%d processPrio=%d", sched_pid, policy, prio);
                        int sched_fd = open(SCHEDINFO_PATH, O_APPEND | O_WRONLY);

                        int init_result = finit_module(sched_fd, schedArgs, 0);
                        close(sched_fd);
                        if(init_result == 0){
                            printf("Successful.\n");
                            insmod_called = 1;
                            insmod_called_with = sched_pid;
                        } else {
                            printf("Unsucessful: %s\n", strerror(errno));
                        }
                    }
                }
            } else {
                // Default commands asked for us to define.
                child = fork();
                if(child==0){
                    pipe_i = pipeIndex(args);
                    if(pipe_i != 0){
                        // In the piped case
                        pid_t pipe_child;
                        pipe(pipefd);

                        if(pipe_child = fork()){
                            // First command
                            close(pipefd[0]);
                            dup2(pipefd[1], STDOUT_FILENO);

                            char **preArgs = malloc(sizeof(args));
                            
                            int i;
                            for(i=0; i < pipe_i; i++){
                                preArgs[i] = args[i];
                            }
                            preArgs[pipe_i] = NULL;

                            executeCommand(preArgs);     
                        } else {
                            // Second command
                            close(pipefd[1]);
                            dup2(pipefd[0], STDIN_FILENO);

                            char **postArgs = malloc(sizeof(args));
                            
                            int i = pipe_i + 1;
                            while(args[i] != NULL){
                                postArgs[i - (pipe_i + 1)] = args[i];
                                i++;
                            }

                            wait(NULL);
                            executeCommand(postArgs);
                        }

                    }else {
                        // No pipe just execute
                        executeCommand(args);
                    }
                    
                    exit(0);
                }
                else{
                    do{
                        if(!background){
                            waitpid(child, &status, WUNTRACED);
                        }
                    } while(!WIFEXITED(status) && !WIFSIGNALED(status));
                }
            }
        }
    }

    if(insmod_called)
        delete_module(SCHEDINFO, 0);

}

int executeCommand(char *args[]){
    int redirectDescriptor;

    char *crontabargs[10];
    char *crontabCommands[1024];
    char line[1024];
    

    redirect(args, &redirectDescriptor);

    if (!strcmp(args[0], "history")) {
        if (!args[1]) {
            int i;
            if(hstCirc < HIST_BUFFER_SIZE){
                // Buffer is not full yet.
                for(i = 0; i < hst; i++)
                    printf("%s\n", history[i]);
            }
            else
            {
                // Buffer overflowed and started rewriting old commands.
                for (i=hst; i<HIST_BUFFER_SIZE; i++)
                    printf("%s\n", history[i]);
                
                for(i = 0; i < hst; i++)
                    printf("%s\n", history[i]);

            }
        }
        else
            printf(" History should not take any argument\n");

        exit(0);
    }

    if(!strcmp(args[0] ,"pwd")){
        execv("/bin/pwd",args);
    }

    if(!strcmp(args[0] ,"ls")){
        execv("/bin/ls",args);
    }
    
    if(!strcmp(args[0] , "trash")&&!strcmp(args[1] , "-l")){
        crontabargs[0]="crontab";
        crontabargs[1]="-l";
        crontabargs[2]=NULL;
        execv("/usr/bin/crontab",crontabargs);

    }else if(!strcmp(args[0] , "trash")&&!strcmp(args[1] , "-r")){
        if (fork()){                            
            FILE *cronfile = fopen(CRONFILE,"r");
            int fileLength = 0;
            while(fgets(line, 128, cronfile) != NULL){
                char * line_cpy = malloc(sizeof(line));
                strcpy(line_cpy, line);
                strtok(line_cpy, " ");
                int j;
                for(j=0; j<5; j++)
                    strtok(NULL, " ");

                char * i_path = strtok(NULL, " ");
                char * c_path = malloc(sizeof(args[2]) + sizeof(char)*4);
                sprintf(c_path, "%s/*\n", args[2]);

                if(strcmp(i_path, c_path)){
                    crontabCommands[fileLength]=malloc(sizeof(line));
                    strcpy(crontabCommands[fileLength], line);
                    fileLength++;
                }

                free(c_path);
            }
            fclose(cronfile);

            cronfile = fopen(CRONFILE, "w");
            int j;
            for(j=0; j < fileLength; j++){
                fprintf(cronfile, "%s", crontabCommands[j]);
            }
            fclose(cronfile);

            crontabargs[0]="crontab";
            crontabargs[1]="-r";
            crontabargs[2]=NULL;
            execv("/user/bin/crontab", crontabargs);
        } else {
            wait(NULL);
            crontabargs[0]="crontab";
            crontabargs[1]=CRONFILE;
            crontabargs[2]=NULL;
            execv("/usr/bin/crontab", crontabargs);
        }

    } else if(!strcmp(args[0] , "trash") && args[1] && args[2]){
        char* second = strtok(args[1],".");
        char* first = strtok(NULL,".");
        FILE *cronfile = fopen(CRONFILE, "a");
        fprintf(cronfile,"%s %s * * * rm %s/*\n", first, second, args[2]);
        fclose(cronfile);
        crontabargs[0]="crontab";
        crontabargs[1]=CRONFILE;
        crontabargs[2]=NULL;
        execv("/usr/bin/crontab", crontabargs);
    }

    if(!strcmp(args[0] ,"echo"))
        execv("/bin/echo", args);

    if(!strcmp(args[0] ,"clear"))
        execv("/usr/bin/clear", args);

    if(!strcmp(args[0] ,"env"))
        execv("/usr/bin/env", args);

    if(!strcmp(args[0] ,"sleep"))
        execv("/bin/sleep", args);

    if(!strcmp(args[0] ,"less"))
        execv("/usr/bin/less", args);

    if(!strcmp(args[0],"grep"))
        execv("/bin/grep", args);

    fprintf(stderr, "%s: is not defined.\n", args[0]);
    return 0;
}


/**
* The parseCommand function below will not return any value, but it will just: read
* in the next command line; separate it into distinct arguments (using blanks as
* delimiters), and set the args array entries to point to the beginning of what
* will become null-terminated, C-style strings.
*/
int parseCommand(char inputBuffer[], char *args[],int *background)
{
    int length,     /* # of characters in the command line */
    i,      /* loop index for accessing inputBuffer array */
    start,      /* index where beginning of next command parameter is */
    ct,         /* index of where to place the next parameter into args[] */
    command_number; /* index of requested command number */

    ct = 0;

    /* read what the user enters on the command line */
    do {
        printf("kush>");
        fflush(stdout);
        length = read(STDIN_FILENO,inputBuffer,MAX_LINE);
    }
    while (inputBuffer[0] == '\n'); /* swallow newline characters */

    /**
    *  0 is the system predefined file descriptor for stdin (standard input),
    *  which is the user's screen in this case. inputBuffer by itself is the
    *  same as &inputBuffer[0], i.e. the starting address of where to store
    *  the command that is read, and length holds the number of characters
    *  read in. inputBuffer is not a null terminated C-string.
    */
    start = -1;
    if (length == 0)
    exit(0);            /* ^d was entered, end of user command stream */

    /**
    * the <control><d> signal interrupted the read system call
    * if the process is in the read() system call, read returns -1
    * However, if this occurs, errno is set to EINTR. We can check this  value
    * and disregard the -1 value
    */

    if ( (length < 0) && (errno != EINTR) ) {
        perror("error reading the command");
        exit(-1);           /* terminate with error code of -1 */
    }

    /**
    * Parse the contents of inputBuffer
    */

    for (i=0;i<length;i++) {
        /* examine every character in the inputBuffer */

        switch (inputBuffer[i]){
            case ' ':
            case '\t' :               /* argument separators */
            if(start != -1){
                args[ct] = &inputBuffer[start];    /* set up pointer */
                ct++;
            }
            inputBuffer[i] = '\0'; /* add a null char; make a C string */
            start = -1;
            break;

            case '\n':                 /* should be the final char examined */
            if (start != -1){
                args[ct] = &inputBuffer[start];
                ct++;
            }
            inputBuffer[i] = '\0';
            args[ct] = NULL; /* no more arguments to this command */
            break;

            default :             /* some other character */
            if (start == -1)
            start = i;
            if (inputBuffer[i] == '&') {
                *background  = 1;
                inputBuffer[i-1] = '\0';
            }
        } /* end of switch */
    }    /* end of for */

    /**
    * If we get &, don't enter it in the args array
    */

    if (*background)
    args[--ct] = NULL;

    args[ct] = NULL; /* just in case the input line was > 80 */

    return 1;

} /* end of parseCommand routine */


int redirect(char *args[], int *fileDescriptor){
    int length=0;
    while(args[length]!=NULL){
        length++; }
        if(length>2&&!strcmp(args[length-2],">>")){
            *fileDescriptor=open(args[length-1], O_CREAT | O_APPEND | O_WRONLY, S_IRUSR | S_IWUSR | S_IXUSR);
            dup2(*fileDescriptor, STDOUT_FILENO);
            args[length-1]=args[length-2]=NULL;
            return 0;
        }if(length>2&&!strcmp(args[length-2],">")){
            *fileDescriptor=open(args[length-1], O_CREAT | O_TRUNC | O_WRONLY, S_IRUSR | S_IWUSR | S_IXUSR);
            dup2(*fileDescriptor, STDOUT_FILENO);
            args[length-1]=args[length-2]=NULL;
            return 0;
        }
        return -1 ;
    }


int pipeIndex(char *args[]){
    int length = 0;
    int i;
    while(args[length] != NULL){
        length++;
    }

    for(i=0; i < length; i++){
        if(!strcmp(args[i], "|"))
            return i; 
    }

    return 0;
}

int finit_module(int fd, const char *param_values, int flags){
    return 0;
}

int delete_module(const char *name, int flags){
    return 0;
}
