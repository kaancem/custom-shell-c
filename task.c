#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/list.h>
#include <linux/types.h>
#include <linux/slab.h>
#include <linux/sched.h>



static int processID = -1;
static int processPolicy = 0;
static int processPrio = 0;

module_param(processID, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
module_param(processPolicy, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
module_param(processPrio, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);


int sched_info_init(void){
	struct task_struct *task; // Our main process
	struct task_struct *parent; // Parent process
	struct list_head *sibling_head; // Sibling list/iterator
	struct list_head *sibling_pos;
	struct task_struct *sibling_task; // Sibling task currently iterated on.
	int change_priority_result;

	printk(KERN_INFO "Loading schedInfo module\n");
	printk(KERN_INFO "Called with processID=%d processPolicy=%d processPrio=%d\n", processID, processPolicy, processPrio);

	for_each_process(task){
		if(task->pid == processID){
			printk(KERN_INFO "Process: %s[%d]\n", task->comm, task->pid);
			printk(KERN_INFO "User ID: %d\n", task->cred->euid);
			printk(KERN_INFO "Dynamic Priority: %d\n", task->prio);
			printk(KERN_INFO "Static Priority: %d\n", task->static_prio);
			printk(KERN_INFO "Policy: %d\n", task->policy);

			parent = task->parent;
			printk(KERN_INFO "Parent: %s[%d]\n", parent->comm, parent->pid);
		
			sibling_head = &(parent->children);
			printk(KERN_INFO "Siblings:\n");

			list_for_each(sibling_pos, sibling_head){
				sibling_task = list_entry(sibling_pos, struct task_struct, sibling);
				if(sibling_task->pid != task->pid){
					printk(KERN_INFO "\t%s[%d]\n", sibling_task->comm, sibling_task->pid);
				}
			}
			/*
			if(processPrio > 0){
				// Passed a valid priority argument
				change_priority_result = setpriority(PRIO_PROCESS, processID, prio);
				if(change_priority_result == -1)
					printk(KERN_INFO "Unable to change priorty.");
				else
					printk(KERN_INFO "Changed process %d priorty to %d.", processID, prio);
				
			}
			*/
		}
	}

	return 0;
}

void sched_info_exit(void){
	printk(KERN_INFO "Removing schedInfo module\n");
}

module_init(sched_info_init);
module_exit(sched_info_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("COMP304 Project 1");
MODULE_AUTHOR("Cem - Kaan");